package io.nae;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.StaticHandler;
import netscape.javascript.JSObject;

public class MainVerticle extends AbstractVerticle {

    @Override
    public void start(Promise<Void> start) {
        vertx.deployVerticle(new HelloVerticle());
        Router router = Router.router(vertx);
        router.get("/api/v1/hello").handler(this::helloVertx);
        router.get("/api/v1/hello/:name").handler(this::helloWithName);

        router.route().handler(StaticHandler.create("web"));

        doConfig(start, router);

    }

    private void doConfig(Promise<Void> start, Router router){
        ConfigStoreOptions defaultConfig = new ConfigStoreOptions()
                .setType("file")
                .setFormat("json")
                .setConfig(new JsonObject().put("path", "config.json"));

        ConfigRetrieverOptions opts = new ConfigRetrieverOptions()
                .addStore(defaultConfig);

        ConfigRetriever cfgRetriever = ConfigRetriever.create(vertx, opts);
        Handler<AsyncResult<JsonObject>> handler = asyncResult -> this.handleConfigRestults(start, asyncResult, router);
        cfgRetriever.getConfig(handler);
    }

    void handleConfigRestults(Promise<Void> start, AsyncResult<JsonObject> asyncResult, Router router){
        if(asyncResult.succeeded()){
            JsonObject config = asyncResult.result();
            JsonObject http = config.getJsonObject("http");
            int httpPort = http.getInteger("port");
            vertx.createHttpServer().requestHandler(router).listen(httpPort );
            start.complete();
        }else{
            // figure out what to do here
            start.fail("Unable to load configuration file");
        }

    }

    void helloVertx(RoutingContext ctx){
        vertx.eventBus().request("hello.vertx.addr", "", reply -> {
            ctx.request().response().end((String) reply.result().body());
        });
    }

    void helloWithName(RoutingContext ctx){
        String name = ctx.pathParam("name");
        vertx.eventBus().request("hello.named.addr", name, reply ->{
            ctx.request().response().end((String) reply.result().body());
        });
    }
}
